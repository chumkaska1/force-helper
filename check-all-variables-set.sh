#!/bin/bash

if [ -z "$1" ]; then
  echo "Read a list of variables and check if all of them are set."
  echo "Usage: $0 <env_file.txt>"
  echo "Empty strings and strings starting with # are ignored."
  exit 1
fi

LIST_FILE="$(readlink -f "$1")"

if [[ ! -f "$LIST_FILE" || ! -r "$LIST_FILE" ]]; then
  echo "Can not open file '$LIST_FILE' for read"
  exit 1
fi

REQUIRED_VARIABLES=$(grep -v '^\(#\|\s*$\)' "$LIST_FILE")

something_empty="0"
for var_name in ${REQUIRED_VARIABLES[@]}; do
  # real bash required, no substitutes
  if [ -z "${!var_name}" ]; then
    echo "Variable ${var_name} must be set"
    something_empty="1"
  fi
done
# catch situation when variable name is invalid
res=$?
if [ "$res" != "0" ]; then
  exit 1
fi

if [ "$something_empty" = "1" ]; then
  echo "Not all required variables are set"
  exit 1
fi
