FROM docker.io/library/debian:buster-slim AS download

ARG AWS_VERSION=2.2.37
ARG KUBECTL_VERSION=1.22.1
ARG YQ_VERSION=4.13.3

RUN set -e; \
    apt update; \
    apt -y --no-install-recommends --no-install-suggests install \
      ca-certificates \
      curl \
      gettext-base \
      unzip; \
    apt clean; \
    curl -fL https://awscli.amazonaws.com/awscli-exe-linux-x86_64-${AWS_VERSION}.zip --output aws.zip; \
    unzip -q aws.zip; \
    rm -f aws.zip; \
    mv /aws/dist/ /awscli; \
    rm -rf /aws/; \
    ln -s /awscli/aws /usr/local/bin/aws; \
    curl -fL https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl --output /usr/local/bin/kubectl; \
    chmod +x /usr/local/bin/kubectl; \
    ln -s /usr/local/bin/kubectl /usr/local/bin/k; \
    curl -fL https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64.tar.gz --output yq.tar.gz; \
    tar xf yq.tar.gz; \
    rm -f yq.tar.gz; \
    mv yq_linux_amd64 yq; \
    chmod +x yq; \
    mv yq /usr/local/bin/

COPY *.sh /usr/local/bin/
