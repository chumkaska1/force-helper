#!/bin/bash

STATUS="$1"
if [ -z "$STATUS" ]; then
  echo "Must supply status message as argument"
  exit 1
fi

if [[ -z "$TELEGRAM_NOTIFY_API_KEY" || -z "$TELEGRAM_NOTIFY_CHAT_ID" ]]; then
  echo "TELEGRAM_NOTIFY_API_KEY and TELEGRAM_NOTIFY_CHAT_ID must be set"
  exit 1
fi

DECODED_API_KEY=$(echo "$TELEGRAM_NOTIFY_API_KEY" | base64 -d)
if [ "$?" != "0" ]; then
  echo "TELEGRAM_NOTIFY_API_KEY must be base64-encoded"
  exit 1
fi

msg="$STATUS ${CI_PROJECT_PATH} *${CI_COMMIT_REF_NAME} | ${CI_COMMIT_MESSAGE}*"
echo "Sending message: '$msg'"

curl -X POST \
  "https://api.telegram.org/bot$DECODED_API_KEY/sendMessage" \
  --data "chat_id=$TELEGRAM_NOTIFY_CHAT_ID&parse_mode=markdown&text=$msg"

if [ "$?" != "0" ]; then
  echo "Failed to send telegram notification"
  exit 1
fi
